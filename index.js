"use strict";

// require(paketti) importtaa paketin
// lataa express-paketti muuttujaan
let express = require("express");
// lataa body-parser-paketti muuttujaan
let bodyp = require("body-parser");
// tee express paketista instanssi
let app = express();
// maaraa palvelimen kuunteluportti
const PORT = 80;

// initialisoi varit
let red = 255,
    green = 255,
    blue = 255;

// pody-parser instanssi hoitaa POST methodin
// requestille bodyn
app.use(bodyp.urlencoded({ extended: false }));
app.use(bodyp.json());

// kuuntele porttia PORT
// kun valmis kutsu toisena argumenttina
// olevaa callback-funktiota
app.listen(PORT, function (){
  console.log("Server started!");
});

// maaraa reitille '/' GET-methodin kasittelija
app.get("/", function(request, response){
  // laheta index.html-tiedosto kayttajalle
  response.sendFile(`${__dirname}/index.html`)

});
